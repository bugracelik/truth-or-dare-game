package com.bugra.truthordaregame.domain;

public class Question {

    private Integer id;
    private String question;
    private Integer level;

    public Question() {
    }

    public Question(Integer id, String question, Integer level) {
        this.id = id;
        this.question = question;
        this.level = level;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", question='" + question + '\'' +
                ", level=" + level +
                '}';
    }
}

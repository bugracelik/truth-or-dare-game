package com.bugra.truthordaregame.repository;

import com.bugra.truthordaregame.domain.Question;
import com.bugra.truthordaregame.model.İnformationDtoForWithoutQuestion;
import com.bugra.truthordaregame.request.CreateQuestionRequest;
import com.bugra.truthordaregame.request.DeleteQuestionByIdRequest;
import com.bugra.truthordaregame.request.UpdateQuestionByIdRequest;
import com.bugra.truthordaregame.rowmapper.QuestionRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class GameRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List getQuestionForLevel(Integer level) {
        return getQuestionListForRequestedLevel(level);
    }

    private List getQuestionListForRequestedLevel(Integer level) {
        switch (level) {
            case 1:
                return getOneEasyQuestionProc();
            case 2:
                return getOneMiddleQuestionProc();
            case 3:
                return getOneHardQuestionProc();
            default:
                return informationMessageForNonExistsLevel();
        }
    }

    private List<İnformationDtoForWithoutQuestion> informationMessageForNonExistsLevel() {
        List<İnformationDtoForWithoutQuestion> i̇nformationDtoList = new ArrayList<>();
        i̇nformationDtoList.add(new İnformationDtoForWithoutQuestion("Aradıgınız zorluk seviyesinde soru yoktur lütfen 1-2-3 kategorilerini yazınız"));
        return i̇nformationDtoList;
    }

    private List<Question> getOneHardQuestionProc() {
        String sql = "SELECT * FROM hardquestion  ORDER BY RAND() LIMIT 1";
        return jdbcTemplate.query(sql, new QuestionRowMapper());
    }

    private List<Question> getOneMiddleQuestionProc() {
        String sql = "SELECT * FROM middlequestion ORDER BY RAND() LIMIT 1";
        return jdbcTemplate.query(sql, new QuestionRowMapper());
    }

    private List<Question> getOneEasyQuestionProc() {
        String sql = "SELECT * FROM easyquestion ORDER BY RAND() LIMIT 1";
        return jdbcTemplate.query(sql, new QuestionRowMapper());
    }

    public boolean addQuestion(CreateQuestionRequest createRequest) {
        boolean addQuestionControlFlag = false;

        if (createRequest.getLevel() == 1) {
            String sqlWord = String.format("insert into easyquestion (question, level) values ('%s', %d)",
                    createRequest.getQuestion(),
                    createRequest.getLevel());
            jdbcTemplate.execute(sqlWord);
            addQuestionControlFlag = true;
        } else if (createRequest.getLevel() == 2) {
            String sqlWord = String.format("insert into middlequestion (question, level) values ('%s', %d)",
                    createRequest.getQuestion(),
                    createRequest.getLevel());
            jdbcTemplate.execute(sqlWord);
            addQuestionControlFlag = true;
        } else if (createRequest.getLevel() == 3) {
            String sqlWord = String.format("insert into hardquestion (question, level) values ('%s', %d)",
                    createRequest.getQuestion(),
                    createRequest.getLevel());
            jdbcTemplate.execute(sqlWord);
            addQuestionControlFlag = true;
        }
        return addQuestionControlFlag;
    }


    public boolean updateQuestion(UpdateQuestionByIdRequest updateRequest) throws SQLException {
        boolean updateQuestionControlFlag = false;

        //eğer id ve level parametlerinde db de soru varsa update işlemi yapılır.
        if (isQuestionExistControl(updateRequest.getId(), updateRequest.getLevel())) {
            if (updateRequest.getLevel() == 1) {
                String sqlWord = String.format("update easyquestion set question = '%s' where id = %d",
                        updateRequest.getQuestion(),
                        updateRequest.getId());
                jdbcTemplate.execute(sqlWord);
                return true;
            }
            if (updateRequest.getLevel() == 2) {
                String sqlWord = String.format("update middlequestion set question = '%s' where id = %d",
                        updateRequest.getQuestion(),
                        updateRequest.getId());
                jdbcTemplate.execute(sqlWord);
                return true;
            }
            if (updateRequest.getLevel() == 3) {
                String sqlWord = String.format("update hardquestion set question = '%s' where id = %d",
                        updateRequest.getQuestion(),
                        updateRequest.getId());
                jdbcTemplate.execute(sqlWord);
                return true;
            }
        }
        //kod bu noktaya gelirse update yapılacak resources bulunamamıştır bu yüzden burada create yapıcam.


        CreateQuestionRequest createRequest = new CreateQuestionRequest(updateRequest.getQuestion(), updateRequest.getLevel());
        updateQuestionControlFlag = addQuestion(createRequest);

        return updateQuestionControlFlag;

    }

    public boolean deleteQuestion(DeleteQuestionByIdRequest deleteRequst) {
        boolean deleteQuestionControlFlag = false;

        //eger silmek istedigi soru varsa silinecektir
        if (isQuestionExistControl(deleteRequst.getId(), deleteRequst.getLevel())) {
            if (deleteRequst.getLevel() == 1) {
                    String sqlWord = String.format("delete from easyquestion where id = %d", deleteRequst.getId());
                    jdbcTemplate.execute(sqlWord);
                    return true;
            }
            if (deleteRequst.getLevel() == 2) {
                    String sqlWord = String.format("delete from middlequestion where id = %d", deleteRequst.getId());
                    jdbcTemplate.execute(sqlWord);
                    return true;
            }
            if (deleteRequst.getLevel() == 3) {
                    String sqlWord = String.format("delete from hardquestion where id = %d", deleteRequst.getId());
                    jdbcTemplate.execute(sqlWord);
                    return true;
            }
        }
        List<İnformationDtoForWithoutQuestion> i̇nformationDtoList = new ArrayList<>();
        i̇nformationDtoList.add(new İnformationDtoForWithoutQuestion("silmek istediginiz soru o zorluk seviyesinde bulunamamıştır"));

        return deleteQuestionControlFlag;
    }


    public boolean isQuestionExistControl(Integer id, Integer level) {
         //Burada verilen id ve levele göre databasede olup olmadıgını kontrol ediyoruz
        boolean questionExistControlFlag = false;
        List<Question> questionList;

        if (level == 1) {
            String sqlWord = String.format("select * from easyquestion where id = %d", id);
            questionList = jdbcTemplate.query(sqlWord, new QuestionRowMapper());
            if (questionList.size() != 0)
                return true;
            return questionExistControlFlag;
        }
        if (level == 2) {
            String sqlWord = String.format("select * from middlequestion where id = %d", id);
            questionList = jdbcTemplate.query(sqlWord, new QuestionRowMapper());
            if (questionList.size() != 0)
                return true;
            return questionExistControlFlag;
        }
        if (level == 3) {
            String sqlWord = String.format("select * from hardquestion where id = %d", id);
            questionList = jdbcTemplate.query(sqlWord, new QuestionRowMapper());
            if (questionList.size() != 0)
                return true;
            return questionExistControlFlag;
        }
        return questionExistControlFlag;
    }
}

package com.bugra.truthordaregame.model;

public class QuestionDto {
    private String question;

    public QuestionDto() {
    }

    public QuestionDto(String question) {
        this.question = question;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }
}

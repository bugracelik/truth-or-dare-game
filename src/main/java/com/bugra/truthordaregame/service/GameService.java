package com.bugra.truthordaregame.service;


import com.bugra.truthordaregame.request.CreateQuestionRequest;
import com.bugra.truthordaregame.repository.GameRepository;
import com.bugra.truthordaregame.request.DeleteQuestionByIdRequest;
import com.bugra.truthordaregame.request.UpdateQuestionByIdRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

@Component
public class GameService {

    @Autowired
    GameRepository gameRepository;

    public List getQuestionForLevel(Integer level) {
        return gameRepository.getQuestionForLevel(level);
    }


    public boolean addQuestion(CreateQuestionRequest createRequest) {
        return gameRepository.addQuestion(createRequest);
    }

    public boolean updateQuestion(UpdateQuestionByIdRequest updateRequest) throws SQLException {
        return gameRepository.updateQuestion(updateRequest);
    }

    public boolean deleteQuestion(DeleteQuestionByIdRequest deleteRequst) {
        return gameRepository.deleteQuestion(deleteRequst);
    }

    public boolean isQuestionExistControl(Integer id, Integer level) {
       return gameRepository.isQuestionExistControl(id, level);
    }
}

package com.bugra.truthordaregame.request;

public class DeleteQuestionByIdRequest {
    private Integer id;
    private Integer level;

    public DeleteQuestionByIdRequest() {
    }

    public DeleteQuestionByIdRequest(Integer id, Integer level) {
        this.id = id;
        this.level = level;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "DeleteRequst{" +
                "id=" + id +
                ", level=" + level +
                '}';
    }
}

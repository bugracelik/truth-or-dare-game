package com.bugra.truthordaregame.request;

public class CreateQuestionRequest {
    private String question;
    private Integer level;

    public CreateQuestionRequest() {
    }

    public CreateQuestionRequest(String question, Integer level) {
        this.question = question;
        this.level = level;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
}

package com.bugra.truthordaregame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TruthOrDareGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(TruthOrDareGameApplication.class, args);
	}

}
